import re
import logging
import pandas as pd
import urllib.request
from pandas_schema import Schema, Column
from pandas_schema.validation import CustomElementValidation
from tqdm import tqdm


def series_validation(number: str):
    try:
        pattern = re.compile("^\d{4}")
        if pattern.match(number):
            return True
        return False
    except ValueError:
        return False


def number_validation(number: str):
    try:
        pattern = re.compile("^\d{6}")
        if pattern.match(number):
            return True
        return False
    except ValueError:
        return False


def validate_data(filename: str):
    logging.debug("Validate file {0}.".format(filename))
    df = pd.read_csv(
        filename,
        names=["PASSP_SERIES", "PASSP_NUMBER"],
        compression='bz2',
        header=0, dtype=str,
    )
    s_validation = [
        CustomElementValidation(lambda i: series_validation(i), 'Passport series is not valid')
    ]
    n_validation = [
        CustomElementValidation(lambda i: number_validation(i), 'Passport number is not valid')
    ]

    schema = Schema([Column('PASSP_SERIES', s_validation), Column('PASSP_NUMBER', n_validation)])
    errors = schema.validate(df)
    errors_index_rows = [e.row for e in errors]
    return df.drop(index=errors_index_rows)


class DownloadProgressBar(tqdm):
    def update_to(self, b=1, bsize=1, tsize=None):
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)


def download_file(url: str) -> str:
    file_name = url.split('/')[-1]
    logging.debug("Download file {0}".format(file_name))
    with DownloadProgressBar(unit='B', unit_scale=True, miniters=1, desc=file_name) as t:
        urllib.request.urlretrieve(url, filename=file_name, reporthook=t.update_to)
    return file_name


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    df1 = validate_data("list_of_expired_passports.csv.bz2")
    df1.to_csv('valid_passports.csv', index=False)
    file2 = download_file("http://models.tegia.ru/person_documents/list_of_expired_passports_20210906.csv.bz2")
    df2 = validate_data(file2)
    merged = pd.merge(df1, df2, how='outer', on=['PASSP_SERIES', 'PASSP_NUMBER'], indicator=True)
    merged.query("_merge=='right_only'").to_csv('exclude.csv', columns=['PASSP_SERIES', 'PASSP_NUMBER'], index=False)
    merged.query("_merge=='left_only'").to_csv('added.csv', columns=['PASSP_SERIES', 'PASSP_NUMBER'], index=False)


